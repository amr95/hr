//
//  LogVC.swift
//  HR
//
//  Created by Amr Saleh on 8/17/20.
//  Copyright © 2020 Amr saleh. All rights reserved.
//

import UIKit

class LogVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.registerCellNib(cellClass: LogTVC.self)
    }
}

extension LogVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue() as LogTVC
        return cell
    }
    
    
}
