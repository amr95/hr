//
//  CustomView.swift
//  HR
//
//  Created by Apple on 8/16/20.
//  Copyright © 2020 Amr saleh. All rights reserved.
//

import UIKit

class CustomView: UIView {
    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit() {
        
        if let nib = Bundle.main.loadNibNamed("CustomView", owner: self),
            let nibView = nib.first as? UIView {
            nibView.frame = bounds
            nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            addSubview(nibView)
        }
        
    }
    
}
