//
//  LoginPresenter.swift
//  HR
//
//  Created by Amr Saleh on 8/16/20.
//  Copyright © 2020 Amr saleh. All rights reserved.
//

import Foundation
import Alamofire

protocol LoginViewDelegate {
    func displayLoading()
    func dismissLoading()
    func displayError()
    func displayError(error: String)
    func segueToMain()
}

class LoginPresenter {
    
    var delegate: LoginViewDelegate?
    
    // attatch view to presenter
    func attatchView(view: LoginViewDelegate){
        delegate = view
    }
    // detach view to presenter
    func detachView(){
        delegate = nil
    }
    
    func startNetworking(email: String , password: String, lat: String, lng: String, deviceId: String) {
        self.delegate?.displayLoading()
        
        let baseUrl = "https://104.248.89.169/api/get-token/"
        let queryStringParam  =  [
            "lat":lat,
            "lng":lng,
            "device_id":deviceId]
        var urlComponent = URLComponents(string: baseUrl)!
        let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value) }
        urlComponent.queryItems = queryItems
        let param = ["username": email,
                     "password": password]
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)
        request.allHTTPHeaderFields = headers
        print(request)
        AF.request(request).responseJSON { resposneData in
            let data = resposneData
            print(data)
            do {
                let obj = try JSONDecoder().decode(LoginModel.self, from: data.data!)
                print("obj = \(obj)")
            } catch {
                print("ERROR")
            }
        }
        
    }
}
