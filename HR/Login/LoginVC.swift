//
//  LoginVC.swift
//  HR
//
//  Created by Amr Saleh on 8/16/20.
//  Copyright © 2020 Amr saleh. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    
    let presenter = LoginPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let presenter = LoginPresenter()
    }
    
    @IBAction func loginButton(_ sender: Any) {
        guard let email = userName.text else {return}
        guard let password = password.text else {return}
        presenter.startNetworking(email: email, password: password,
                                  lat: "30.0961583", lng: "31.3102967", deviceId: "aa")
    }
    
}
