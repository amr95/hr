////
////  APIRouter.swift
////  HR
////
////  Created by Amr Saleh on 8/16/20.
////  Copyright © 2020 Amr saleh. All rights reserved.
////
//
//import Foundation
//import Alamofire
//
//enum APIRouter: URLRequestConvertible {
//    case login(email: String, password: String, lat: String, lng: String, deviceId: String)
//    
//    // MARK: - Http Methoda
//    
//    var method: HTTPMethod {
//        switch self {
//        case .login:
//            return .post
//        }
//    }
//    
//    var parameters: Parameters? {
//        switch self {
//        case .login(let email, let password , let lat, let lng, let deviceId):
//            return ["username": email, "password": password, "lat":lat, "lng":lng, "device_id": deviceId]
//        }
//    }
//    
//    var path: String {
//      
//      switch self {
//      case .login:
//        return "get-token/"
//        }
//    }
//    
//    var encoding: ParameterEncoding {
//        return URLEncoding.default
//    }
//    
//
//    var headers: HTTPHeaders? {
//      switch self {
//      case .login:
//        return ["Content-Type": "application/json"]
//        
//      }
//    }
//    
//    func asURLRequest() throws -> URLRequest {
//        let url = try Constants.baseURL.asURL().appendingPathComponent(path)
//        print("url = \(url)")
//        var request = URLRequest(url: url)
//        request.httpMethod = method.rawValue
//        
//        if let headers = headers {
//          for (headerField, headerValue) in headers {
//            request.setValue(headerValue, forHTTPHeaderField: headerField)
//          }
//        }
//        
//        if let parameters = parameters {
//          return try encoding.encode(request, with: parameters)
//        }
//        
//        return request
//    }
//    
//    
//}
//struct Constants {
//  static let baseURL = "https://104.248.89.169/api/get-token/"
//}
