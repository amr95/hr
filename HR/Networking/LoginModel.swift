//
//  LoginModel.swift
//  HR
//
//  Created by Amr Saleh on 8/16/20.
//  Copyright © 2020 Amr saleh. All rights reserved.
//

import Foundation

// MARK: - LoginModel
struct LoginModel: Decodable {
    let token: String?
    let id: Int?
    let username, name: String?
    let userType, teamTag: Int?
    let photo, authAreas, currentArea: String?
    let deviceID: String?
    let deviceValidation: Bool?
    let userCategoryID: Int?

    enum CodingKeys: String, CodingKey {
        case token, id, username, name
        case userType
        case teamTag
        case photo
        case authAreas
        case currentArea
        case deviceID
        case deviceValidation
        case userCategoryID
    }
}
