//
//  ViewController.swift
//  HR
//
//  Created by Apple on 8/16/20.
//  Copyright © 2020 Amr saleh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var manager: CustomView!
    @IBOutlet weak var report: CustomView!
    
    @IBOutlet weak var log: CustomView!
    @IBOutlet weak var hr: CustomView!
    @IBOutlet weak var policy: CustomView!
    @IBOutlet weak var myTeam: CustomView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        manager.icon.image = #imageLiteral(resourceName: "boss")
        manager.name.text = "المدير المباشر لك"
        
        report.icon.image = #imageLiteral(resourceName: "submit_somthing_img")
        report.name.text = "ارسال شكوي"
        
        log.icon.image = #imageLiteral(resourceName: "checkin_btn_logo")
        log.name.text = "سجل حضوري و انصرافي"
        
        hr.icon.image = #imageLiteral(resourceName: "hr")
        hr.name.text = "مسؤل الموارد البشرية"
        
        policy.icon.image = #imageLiteral(resourceName: "ticket_history_img")
        policy.name.text = "سياسات الشركة"
        
        myTeam.icon.image = #imageLiteral(resourceName: "ticket_history_img")
        myTeam.name.text = "فريق العمل الخاص بي"
        
    }


}

